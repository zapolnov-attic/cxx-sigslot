
SigSlot
=======

This is the signals / slots library for the C++ language.


Building
========

This library is not intended to be built directly. Instead it is supposed
to be included into projects using the *buildtool*. Please consult the
[buildtool](https://github.com/zapolnov/buildtool) documentation for more
information.


Usage
=====

Usage is simple.

First you should declare a signal using the Signal class:

``` c++

using SigSlot::Signal;

Signal<> myTestSignal;
Signal<int, float> myTestSignal2;
```

Then you can connect functions or methods to the signal. To connect methods,
they should be declared in the class that is a direct or indirect child of
the HasSlots class:

``` c++

/* Connecting functions */

void func();

myTestSignal.connect(func);


/* Connecting classes */

using SigSlot::HasSlots;

class A : public HasSlots
{
public:
	void slot(int, float);
};

A a;
myTestSignal2.connect(&a, &A::slot);
```

To send a signal use the call operator:

``` c++

myTestSignal();
myTestSignal2(1, 2.3f);
```


License
=======

Copyright © 2014 Nikolay Zapolnov (zapolnov@gmail.com).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
