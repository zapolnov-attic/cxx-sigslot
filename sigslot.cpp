/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "sigslot.h"
#include <stdexcept>

SigSlot::RawSignal::~RawSignal()
{
	disconnectAllSlots();
}

void SigSlot::RawSignal::connect(HasSlots * target, RawCallback * callback)
{
	if (m_Emitting)
		throw std::runtime_error("it is not allowed to connect slot to signal while emitting the signal.");

	target->m_Senders.insert(this);
	std::pair<ConnectionsMap::iterator, bool> r = m_Connections.insert(std::make_pair(target, callback));
	if (!r.second)
	{
		if (!r.first->second)
			r.first->second = callback;
		else
			delete callback;
	}
}

void SigSlot::RawSignal::disconnect(HasSlots * target)
{
	ConnectionsMap::iterator it = m_Connections.find(target);
	if (it != m_Connections.end())
	{
		delete it->second;
		it->second = NULL;
		target->m_Senders.erase(this);
	}
	cleanup();
}

void SigSlot::RawSignal::disconnectAllSlots()
{
	for (ConnectionsMap::iterator it = m_Connections.begin(); it != m_Connections.end(); ++it)
	{
		delete it->second;
		it->second = NULL;
		it->first->m_Senders.erase(this);
	}
	cleanup();
}

void SigSlot::RawSignal::cleanup()
{
	if (m_Emitting)
		return;

	for (ConnectionsMap::iterator it = m_Connections.begin(); it != m_Connections.end(); )
	{
		if (it->second)
			++it;
		else
			it = m_Connections.erase(it);
	}
}

/* ========== */

SigSlot::HasSlots::HasSlots()
{
}

SigSlot::HasSlots::HasSlots(const HasSlots &)
{
}

SigSlot::HasSlots::~HasSlots()
{
	disconnectFromAllSignals();
}

SigSlot::HasSlots & SigSlot::HasSlots::operator=(const HasSlots &)
{
	return *this;
}

void SigSlot::HasSlots::disconnectFromAllSignals()
{
	for (;;)
	{
		SenderSet::iterator it = m_Senders.begin();
		if (it == m_Senders.end())
			return;

		RawSignal * sender = *it;
		sender->disconnect(this);
		m_Senders.erase(sender);
	}
}
