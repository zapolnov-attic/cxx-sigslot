/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __91a7946a76541306acf3e352f81e703d__
#define __91a7946a76541306acf3e352f81e703d__

#include "callback.h"
#include UNORDERED_H

namespace SigSlot
{
	class HasSlots;

	class RawSignal
	{
	public:
		void connect(HasSlots * target, RawCallback * callback);

		void disconnect(HasSlots * target);
		void disconnectAllSlots();

	protected:
		class EmitLocker
		{
		public:
			inline EmitLocker(const RawSignal * signal) : m_Signal(signal) { ++m_Signal->m_Emitting; }
			inline ~EmitLocker() { --m_Signal->m_Emitting; }

		private:
			const RawSignal * m_Signal;

			EmitLocker(const EmitLocker &);
			EmitLocker & operator=(const EmitLocker &);
		};

		typedef UNORDERED_MAP<HasSlots *, RawCallback *> ConnectionsMap;
		mutable ConnectionsMap m_Connections;

		inline RawSignal() : m_Emitting(0) {}
		virtual ~RawSignal();

	private:
		mutable int m_Emitting;

		void cleanup();

		RawSignal(const RawSignal &);
		RawSignal & operator=(const RawSignal &);

		friend class EmitLocker;
	};

	class HasSlots
	{
	public:
		void disconnectFromAllSignals();

	protected:
		HasSlots();
		HasSlots(const HasSlots &);
		virtual ~HasSlots();

		HasSlots & operator=(const HasSlots &);

	private:
		typedef UNORDERED_SET<RawSignal *> SenderSet;
		SenderSet m_Senders;

		friend class RawSignal;
	};

	#include "sigslot_generated.h"
}

#endif
