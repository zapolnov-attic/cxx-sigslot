
SUBPROJECT("cxx-sigslot")

if not HAS_SUBPROJECT("cxx-util") then
	error("This library depends on the 'cxx-util' library.")
end

DEFINES {
	"SIGSLOT_H=\"" .. CURRENT_SOURCE_DIR .. "/sigslot.h\"",
}

SOURCE_FILES {
	"callback.h",
	"sigslot.cpp",
	"sigslot.h",
	"sigslot_generated.h",
}
